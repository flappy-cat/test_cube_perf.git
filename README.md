# test_cube_perf 

#### 介绍
压测Ascend芯片的计算性能，算子程序可以运行在所有平台，但是统计指标仅针对Ascend310，并且代码仅作计算性能实测展示。
参数指标是left_matrix[8,128,16] * right_matrix[8,128,16]
本代码仓包含了算子工程文件集，以及acl测试单算子模型的代码，最后可以输出
![输入图片说明](https://images.gitee.com/uploads/images/2021/0314/092140_c483270e_2050080.jpeg "cube_performance.jpg")
表示，计算压测的结果XXXFLOPS,比例是能与标称值的接近程度。
#### 安装教程
1.  git clone https://gitee.com/flappy-cat/test_cube_perf.git
2.  ./build.sh  可以编译算子并在build_out中生成run包
3.  ./build_out/custom_opp_centos_x86_64.run，安装自定义算子的run包
4. 转单算子模型，首先根据算子的IR信息写json文件
    在test_acl_singleop目录中，test_cube_perf.json即为单算子json文件
5.  切换到该目录中，运行./atc_singpleop_cmd.sh，可以生成0_TestCubePerf_1_2_8_128_16_1_2_8_128_16_0_2_8_128_16.om单算子模型
6. 切换到msame中，运行./build.sh，在上一层目录中生成main执行文件
7. 在test_acl_singleop中执行./go_test_perf.sh,在打印信息中可以看到性能指标。
#### 其他
需要注意的是
找到你环境中 Ascend/atc/include 的路径，将该路径设置成ASCEND_TENSOR_COMPLIER_INCLUDE
demo：[export ASCEND_TENSOR_COMPLIER_INCLUDE=/usr/local/Ascend/atc/include]