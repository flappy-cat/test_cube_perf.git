# Copyright 2019 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
test_cube_perf
"""
# pylint: disable=wildcard-import,unused-wildcard-import
from te import tik
from te import platform as cce
import te.platform as tbe_platform

def test_cube_perf(input_x,
                   input_y,
                   output_z,
                   kernel_name="test_cube_perf"):
    """
    test_cube_perf

    Parameters
    ----------
    input_x: dict
        Left matrix of matmul calculation.
    input_y: dict
        Right matrix of matmul calculation.
    kernel_name: str
        cce kernel name, default value is "test_cube_perf"

    returns
    -------
    None
    """
    tik_instance = tik.Tik(tik.Dprofile(), disable_debug=False)
    core_num = tbe_platform.get_soc_spec(tbe_platform.CORE_NUM)
    shape_x = input_x.get("shape")
    shape_y = input_y.get("shape")
    shape_z = output_z.get("shape")
    K1, M, K0 = shape_x
    _, N, _ = shape_y
    input_x = tik_instance.Tensor("float16", shape_x, name="input_x", scope=tik.scope_gm)
    input_y = tik_instance.Tensor("float16", shape_y, name="input_y", scope=tik.scope_gm)
    output_z = tik_instance.Tensor("float32", shape_z, name="output_z", scope=tik.scope_gm)
    with tik_instance.for_range(0, core_num, block_num=core_num):
        mat_a_l1 = tik_instance.Tensor("float16", shape_x, name="mat_a_l1", scope=tik.scope_cbuf)
        mat_b_l1 = tik_instance.Tensor("float16", shape_y, name="mat_b_l1", scope=tik.scope_cbuf)
        dst_l1_out = tik_instance.Tensor("float32", shape_z, name="dst_l1_out", scope=tik.scope_cbuf_out)
        tik_instance.data_move(mat_a_l1[0], input_x[0], 0, 1, K1 * M // 16, 0, 0)
        tik_instance.data_move(mat_b_l1[0], input_y[0], 0, 1, K1 * N // 16, 0, 0)
        with tik_instance.for_range(0, 100000, thread_num=1):
            tik_instance.matmul(dst_l1_out, mat_a_l1, mat_b_l1, K1 * K0, M, N)
            # Only for cube performance test, so comment data out process
            # tik_instance.fixpipe(output_z, dst_l1_out, 8, 4, 0, 0, extend_params={"relu" : True})
    tik_instance.BuildCCE(kernel_name,
                          inputs=[input_x, input_y],
                          outputs=[output_z])
    return tik_instance
