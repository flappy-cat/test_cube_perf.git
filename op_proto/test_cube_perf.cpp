#include "test_cube_perf.h"
namespace ge {

    // ---------------TestCubePerf Op Start-------------------
    IMPLEMT_COMMON_INFERFUNC(TestCubePerfInferShape) {
        Shape input_shape = op.GetInputDesc(0).GetShape();
        DataType input_dtype = op.GetInputDesc(0).GetDataType();
        TensorDesc td = op.GetOutputDesc(0);
        vector<int64_t> y_shape;
        td.SetShape(ge::Shape(input_shape));
        td.SetDataType(input_dtype);
        (void)op.UpdateOutputDesc("output_z", td);
        return GRAPH_SUCCESS;
    }


    IMPLEMT_VERIFIER(TestCubePerf, TestCubePerfVerify)
    {
        return GRAPH_SUCCESS;
    }

    COMMON_INFER_FUNC_REG(TestCubePerf, TestCubePerfInferShape);
    VERIFY_FUNC_REG(TestCubePerf, TestCubePerfVerify);

}  // namespace ge
